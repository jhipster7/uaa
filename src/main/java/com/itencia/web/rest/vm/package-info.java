/**
 * View Models used by Spring MVC REST controllers.
 */
package com.itencia.web.rest.vm;
